package com.example.bmi;

import org.springframework.boot.test.context.SpringBootTest;
import com.example.bmi.model.Body;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

@SpringBootTest
public class BodyTest {

    Body body = new Body(25, "male", 88, 182, 40, 90, 85);

    @Test
    public void testBodyFat(){


        assertEquals(body.getBodyFat(),18, 1);
        body.setNeck(44);
        assertEquals(body.getBodyFat(),15, 1);
        body.setSex("female");
        assertEquals(body.getBodyFat(),19, 1);
        body.setHip(105);
        assertEquals(body.getBodyFat(),29, 1);
    }

    @Test
    public void testHeightDiff(){


        assertEquals(body.getInchDiff(220), 26.5, 0.5);
        assertEquals(body.getInchDiff(187), 13.5, 0.5);
        assertEquals(body.getInchDiff(173), 8, 0.5);
        assertEquals(body.getInchDiff(152.4), 0, 0.00000000000001);
    }

    @Test
    public void testIdealBody(){


        assertEquals(body.idealBodyFat(), 10, 1);
        body.setSex("female");
        assertEquals(body.idealBodyFat(), 18, 1);
        body.setAge(35);
        assertEquals(body.idealBodyFat(), 21, 1);

    }

    @Test
    public void testNeededFat() {
        assertEquals(body.neededFat(), -7, 1);
        body.setAge(55);
        assertEquals(body.neededFat(), 3, 1);
        body.setSex("female");
        assertEquals(body.neededFat(), 5, 1);
        body.setAge(25);
        assertEquals(body.neededFat(), -3, 1);
    }

    @Test
    public void testYourCalories(){
        body.setExercise(4);
        assertEquals(body.getYourCalories(), 3050 , 25);
        body.setExercise(1);
        assertEquals(body.getYourCalories(), 2200 , 25);
        body.setAge(44);
        assertEquals(body.getYourCalories(), 2110 , 25);
        body.setHeight(160);
        assertEquals(body.getYourCalories(), 2000 , 25);
        body.setWeight(54);
        assertEquals(body.getYourCalories(), 1625 , 25);
        body.setSex("female");
        assertEquals(body.getYourCalories(), 1500 , 25);





    }
}
