package com.example.bmi.controller;


import com.example.bmi.model.Body;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DecimalFormat;


@Controller
public class HomeController {

    private static DecimalFormat df1 = new DecimalFormat(".##");
    private static DecimalFormat df2 = new DecimalFormat(".##kg");
    private static DecimalFormat df3 = new DecimalFormat(".##%");

    @GetMapping("home")
    public String home(){
        return "home";
    }

    @GetMapping("home/bmi")
    public String bmiCal(){
        return "bmi";
    }
    @RequestMapping("/getBmi")
    public String yourBmi(@RequestParam(required = false) int age, @RequestParam(required = false) String sex,
                          @RequestParam(required = false) double weight, @RequestParam(required = false) double height, ModelMap modelMap){
        Body body = new Body(age, sex, weight, height);
        double bmi = (body.getWeight())/Math.pow((body.getHeight()/100),2);
        modelMap.addAttribute("bmi", df1.format(bmi));
        return "bmi";
    }

    @GetMapping("home/calorie")
    public String calorieCal(){
        return "calorie";
    }

    @GetMapping("home/bfp")
    public String bfpCal(){
        return "bfp";
    }

    @RequestMapping("/getBFP")
    public String yourBFP(@RequestParam(required = false) int age, @RequestParam(required = false) String sex,
                          @RequestParam(required = false) double weight, @RequestParam(required = false) double height,
                          @RequestParam(required = false) double neck, @RequestParam(required = false) double waist,
                          @RequestParam(required = false) double hip, ModelMap modelMap){
        Body body = new Body(age, sex, weight, height, neck, waist, hip);
        double bfpValue, fatMass, leanMass;
        bfpValue = body.getBodyFat();
        fatMass = (bfpValue * body.getWeight())/100;
        leanMass = body.getWeight() - fatMass;
        modelMap.addAttribute("fatMass", df2.format(fatMass));
        modelMap.addAttribute("leanMass", df2.format(leanMass));
        modelMap.addAttribute("bfpValue", df3.format(bfpValue/100));
        modelMap.addAttribute("idealBodyFat",df3.format(body.idealBodyFat()/100));
        modelMap.addAttribute("neededFat",df2.format(body.neededFat()));
        return "bfp";
    }

    @GetMapping("home/ideal")
    public String idealCal(){
        return "ideal";
    }

    @RequestMapping("/getIdeal")
    public String yourIdeal(@RequestParam(required = false) int age, @RequestParam(required = false) String sex,
                             @RequestParam(required = false) double height, ModelMap modelMap){
        Body body = new Body(age, sex, height);
        double hamwi, devine, robinson, miller;
        if(body.getSex().equals("female")){
            hamwi = 45.5 + body.getInchDiff(body.getHeight()) * 2.2;
            devine = 45.5 + body.getInchDiff(body.getHeight()) * 2.3;
            robinson = 49.0 + body.getInchDiff(body.getHeight()) * 1.7;
            miller = 53.1 + body.getInchDiff(body.getHeight()) * 1.36;
        } else{
            hamwi = 48 + body.getInchDiff(body.getHeight()) * 2.7;
            devine = 50 + body.getInchDiff(body.getHeight()) * 2.3;
            robinson = 52 + body.getInchDiff(body.getHeight()) * 1.9;
            miller = 56.2 + body.getInchDiff(body.getHeight()) * 1.41;
        }
        modelMap.addAttribute("hamwi", df2.format(hamwi));
        modelMap.addAttribute("devine", df2.format(devine));
        modelMap.addAttribute("robinson", df2.format(robinson));
        modelMap.addAttribute("miller", df2.format(miller));
        return "ideal";
    }

    @RequestMapping("/getCalories")
    public String yourCalories(@RequestParam(required = false) int age, @RequestParam(required = false) String sex,
                          @RequestParam(required = false) double weight, @RequestParam(required = false) double height, @RequestParam(required =  false) double exercise, ModelMap modelMap){
        Body body = new Body(age, sex, weight, height, exercise);
        double calorie = body.getYourCalories();
        modelMap.addAttribute("calorie", df1.format(calorie));
        return "calorie";
    }
}
