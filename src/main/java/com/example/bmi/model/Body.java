package com.example.bmi.model;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Body {
    private int age;
    private String sex;
    private double weight;
    private double height;
    private double neck;
    private double waist;
    private double hip;
    private double exercise;

    public Body(int age, String sex, double weight, double height, double neck, double waist, double hip) {
        this.age = age;
        this.sex = sex;
        this.weight = weight;
        this.height = height;
        this.neck = neck;
        this.waist = waist;
        this.hip = hip;
    }

    public Body(int age, String sex, double weight, double height, double exercise) {
        this.age = age;
        this.sex = sex;
        this.weight = weight;
        this.height = height;
        this.exercise = exercise;
    }

    public Body(int age, String sex, double weight, double height) {
        this.age = age;
        this.sex = sex;
        this.weight = weight;
        this.height = height;
    }

    public Body(int age, String sex, double height) {
        this.age = age;
        this.sex = sex;
        this.height = height;
    }

    public double getInchDiff(double height){
        double diff = 0;
        while(height >= 152.4){
            height -= 2.54;
            diff ++;
        }
        diff += (height - 152.4)/2.54;
        return diff;
    }

    public double getBodyFat(){
        double bfpValue;
        if(sex.equals("female")) {
            bfpValue = (495 / (1.29579 - (0.35004 * Math.log10(waist + hip - neck)) + 0.22100 * Math.log10(height))) - 450;
        } else{
            bfpValue = (495 / (1.0324 - (0.19077 * Math.log10(waist - neck)) + 0.15456 * Math.log10(height))) - 450;
        }
        return bfpValue;
    }

    public double idealBodyFat(){
        double idealBodyFat;
    if(sex.equals("female")) {
        idealBodyFat = 170.2773 - 22.91414*age + 1.314437*(Math.pow(age,2)) - 0.03615956*(Math.pow(age,3)) + 0.0004831235*(Math.pow(age,4)) - 0.000002512821*(Math.pow(age,5));
    }
    else{
        idealBodyFat = -15.99075 + 2.185806*age - 0.06456439*(Math.pow(age,2)) + 0.0008984848*(Math.pow(age,3)) - 0.000004090909*(Math.pow(age,4));
    }
    return idealBodyFat;
    }
    public double neededFat(){
        double neededFat;
        neededFat = ((weight/100) *(idealBodyFat() - getBodyFat()))/(1 - idealBodyFat()/100);
        return neededFat;
    }

    public double getYourCalories(){
        double yourCalories, basicCalories;
        if(sex.equals("female")) {
           basicCalories = 10*weight + 6.25*height -5*age -161;
        }
        else{
            basicCalories = 10*weight + 6.25*height -5*age + 5;
        }
        yourCalories = basicCalories + 378.3333*exercise - 81.41667*(Math.pow(exercise,2)) + 31.29167*(Math.pow(exercise,3)) - 5.583333*(Math.pow(exercise,4)) + 0.375*(Math.pow(exercise,5));
        return yourCalories;
    }

}
